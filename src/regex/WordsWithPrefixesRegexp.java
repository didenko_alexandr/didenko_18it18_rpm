package regex;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class WordsWithPrefixesRegexp {

    public static void main(String[] args) throws IOException {
        String s;

        Pattern pattern = Pattern.compile("(?:при|пре)\\S+");
        Matcher matcher = pattern.matcher("");


        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("C:\\Users\\Александр\\IdeaProjects\\Didenko_18IT18_RPM\\src\\regex\\input.txt"))) {
            while ((s = bufferedReader.readLine()) != null) {
                matcher.reset(s);
                while (matcher.find()) {
                    System.out.println(matcher.group());
                    try(FileWriter bufferedWriter = new FileWriter("C:\\Users\\Александр\\IdeaProjects\\Didenko_18IT18_RPM\\src\\regex\\output.txt", true)) {

                        bufferedWriter.write(matcher.group() + "\n");

                    }
                }
            }
        }
    }
}

