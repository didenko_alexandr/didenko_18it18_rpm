package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RealNumberRegexp {
    public static void main(String[] args) {
            String string = "2.5 -5.78 плюс + +67 .8 9. +. 23.12e+10 75.84e+8";
            Pattern pattern = Pattern.compile("([+-]?(\\d*\\.\\d+)|(\\d+\\.\\d*))\\W");
        Pattern pattern1 = Pattern.compile("([+-]?(\\d*\\.\\d+[Ee][+-]?[\\d+])|(\\d+\\.\\d*[Ee][+-]?[\\d+]))");

        System.out.println("Числа с фиксированной точкой: " + regularNumber(string, pattern));
        System.out.println("Числа с плавающей точкой: " + regularNumber(string, pattern1));
    }

    private static String regularNumber(String string, Pattern pattern1) {
        Matcher matcher = pattern1.matcher(string);
        String x = "";
        while (matcher.find()) {
            x += matcher.group();
        }
        return x;
    }
}
