package book;

public enum Genre {
    ХОРОР("хорор"),
    ФАНТАСТИКА("фантастика"),
    РОМАН("роман"),
    ДЕТЕКТИВ("детектив");

    private String favoriteGenre;

    Genre(String genre) {
        this.favoriteGenre = genre;
    }

    public String getFavoriteGenre() {
        return favoriteGenre;

    }
}
