package book;

public class Book {
    private String author;
    private String title;
    private int year;
    private Genre genre;
    private String way;

    public Book(String author, String title, int year, Genre genre, String way) {
        this.author = author;
        this.title = title;
        this.year = year;
        this.genre = genre;
        this.way = way;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public Genre getGenre() {
        return genre;
    }

    public String getWay() {
        return way;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", year=" + year +
                ", genre=" + genre +
                ", way='" + way + '\'' +
                '}';
    }
}
