package book;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        Book book1 = new Book("Демин", "Кристмас", 2009, Genre.ХОРОР, "D:\\Work\\OOP\\src\\ru\\dav\\books\\cristmas.doc");
        Book book2 = new Book("Акунин", "Азазель", 1988, Genre.ДЕТЕКТИВ, "D:\\Work\\OOP\\src\\ru\\dav\\books\\azazel.doc");
        Book book3 = new Book("Кинг", "Сияние", 1977, Genre.ХОРОР, "D:\\Work\\OOP\\src\\ru\\dav\\books\\sianie.doc");
        Book book4 = new Book("Прах", "Кофейня", 2016, Genre.РОМАН, "D:\\Work\\OOP\\src\\ru\\dav\\books\\kofeinya.doc");
        Book book5 = new Book("Браун", "Инферно", 2013, Genre.ФАНТАСТИКА, "D:\\Work\\OOP\\src\\ru\\dav\\books\\inferno.doc");

        ArrayList<Book> books = new ArrayList<>();

        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);
        books.add(book5);

        ArrayList<Book> arrayFavoriteGenres = new ArrayList<>();
        ArrayList<Book> arrayFavoriteAuthor = new ArrayList<>();

        System.out.println("Сортировка книг по фамилиям авторов:");
        sortSurname(books);
        System.out.println(" ");
        System.out.println("Сортировка по дате выхода от новинок к более старым:");
        sortYear(books);
        System.out.println(" ");
        System.out.println("Введите жанр: ");
        String genre = sc.nextLine();
        System.out.println(genre(books, arrayFavoriteGenres, genre));
        System.out.println("Введите автора: ");
        String favorite_author = sc.nextLine();
        System.out.println(author(books, arrayFavoriteAuthor, favorite_author));
        System.out.println(searchTitle(books));

    }

    private static void sortSurname(ArrayList<Book> books) {
        for (int i = 0; i < books.size(); i++) {
            boolean tmp = false;
            for (int j = 0; j < books.size() - i - 1; j++) {
                if (books.get(j).getAuthor().compareTo(books.get(j + 1).getAuthor()) > 0) {

                    tmp = true;
                    Book temp = books.get(j);
                    books.set(j, books.get(j + 1));
                    books.set(j + 1, temp);
                }
            }
            if (!tmp) {
                break;
            }
        }
        for (Book value : books)
            System.out.print(value + "\n");
    }

    public static void sortYear(ArrayList<Book> books) {
        for (int number = 0; number < books.size(); number++) {
            int max = books.get(number).getYear();
            int maxNum = number;
            for (int num = number + 1; num < books.size(); num++) {
                if (books.get(num).getYear() > max) {
                    max = books.get(num).getYear();
                    maxNum = num;
                }
            }
            Book replace = books.get(number);
            books.set(number, books.get(maxNum));
            books.set(maxNum, replace);
        }
        for (Book value : books)
            System.out.print(value + "\n");
        }

    private static ArrayList<Book> genre(ArrayList<Book> books, ArrayList<Book> arrayFavoriteGenres, String genre) {

        for (Book book : books) {
            if (genre.equalsIgnoreCase(book.getGenre().getFavoriteGenre())) {
                arrayFavoriteGenres.add(book);
            }
        }
        return arrayFavoriteGenres;
    }
    private static ArrayList<Book> author(ArrayList<Book> books, ArrayList<Book> arrayFavoriteAuthor, String favorite_author) {
        for (Book book : books) {
            if (favorite_author.equalsIgnoreCase(book.getAuthor())) {
                arrayFavoriteAuthor.add(book);
            }
        }
        return arrayFavoriteAuthor;
    }

    public static Object searchTitle(ArrayList<Book> books) {
        System.out.println("Введите название книги: ");
        String index = sc.nextLine();
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getTitle().equals(index)) {
                return books.get(i);
            }
        }
        return "Такой книги в библиотеке нет!";
    }
    }