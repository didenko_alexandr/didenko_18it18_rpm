package refactoringWorkError;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс, демонстрирующий поиск индекса введённой константы
 *
 * @author А.В.Диденко 18ИТ18
 */
public class RefactoringWorkError {

    public static final int MIN = -25;
    public static final int MAX = 51;

    public static void main(String[] args) {
        int[] array = new int[size("Введите размер массива: ")];
        random(array);
        System.out.println(Arrays.toString(array));
        print(searchIndex(array, size("Введите элемент, индекс которого хотите найти: ")));
    }

    /**
     * Метод, возвращающий число, введёное пользователем
     * @param message выводимое сообщение пользователю
     * @return возращается число, введёное пользователем
     */
    public static int size(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(message);
        return scanner.nextInt();
    }

    /**
     *Метод, заполняющий массив рандомными числами от -25 до 25
     * @param array пустой массив
     */
    public static void random(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN + (int) (Math.random() * MAX);
        }
    }

    /**
     * Метод, реализующий поиск индекса введённого элемента
     * @param array массив
     * @param number число пользователя
     * @return индекс числа пользователя
     */
    public static int searchIndex(int[] array, int number) {
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                index = i;
                break;
            }
        }
        return index;
    }

    /**
     *Метод, выводящий сообщение пользователю с найденным или не найденным индексом
     * @param index индекс числа пользователя
     */
    public static void print(int index) {
        if (index == -1) {
            System.out.println("Нет такого значения!");
        } else {
            System.out.println("Индекс элемента в последовательности - " + (index + 1));
        }
    }
}
