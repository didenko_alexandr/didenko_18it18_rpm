package smileyReplayer;

import smileyReplayer.printers.ConsolePrinter;
import smileyReplayer.printers.DecoratePrinter;
import smileyReplayer.printers.IPrinter;
import smileyReplayer.readers.IReader;
import smileyReplayer.readers.ScannerText;
import smileyReplayer.readers.FileReader;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        IReader reader = new ScannerText();
        IReader readerFile = new FileReader();
        IPrinter printer = new ConsolePrinter();
        IPrinter decoratePrint = new DecoratePrinter();
        Replacer replacer = new Replacer(reader, printer);
        Replacer advReplacer = new Replacer(reader, decoratePrint);
        Replacer replacerFile = new Replacer(readerFile, decoratePrint);
        replacer.replace();
        advReplacer.replace();
        replacerFile.replace();
    }
}

