package smileyReplayer.readers;

import java.util.Scanner;

public class ScannerText implements IReader {
    Scanner scanner = new Scanner(System.in);

    @Override
    public String read() {
        System.out.println("Введите текст: ");
        String text = scanner.nextLine();

        return text;
    }
}
