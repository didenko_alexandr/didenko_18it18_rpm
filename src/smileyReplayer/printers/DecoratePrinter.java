package smileyReplayer.printers;

public class DecoratePrinter implements IPrinter {

    @Override
    public void print(String text) {
        System.out.println("*****\n" + text + "\n*****\n");
    }
}

