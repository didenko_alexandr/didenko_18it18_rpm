package smileyReplayer;

import smileyReplayer.printers.IPrinter;
import smileyReplayer.readers.IReader;

import java.io.IOException;

public class Replacer {
    private IReader reader;
    private IPrinter printer;

    public Replacer(IReader reader, IPrinter printer) {
        this.reader = reader;
        this.printer = printer;
    }

    public void replace() throws IOException {
        final String text = reader.read();
        final String replacedText = text.replaceAll(":\\(", ":-)");
        printer.print(replacedText);
    }
}