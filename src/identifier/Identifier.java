package identifier;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static java.nio.charset.StandardCharsets.UTF_8;

public class Identifier {
    public static void main(String[] args) {
        System.out.println("Ввыводим текст кода в консоль: ");
        String result = "";
        try {
            System.out.println(readere());
            result = readere();
        } catch (IOException e) {
            System.out.println("Файл не найден!");
        }

        System.out.println("\n" + "Выводим все идентификаторы: ");
        System.out.println(regular(result));

        System.out.println("\n" + "Выводим уникальные идентификаторы (без повторений): ");
        System.out.println(regular1( result));
    }

    static String readere() throws IOException {
        Path path = Paths.get("C:\\Users\\Александр\\IdeaProjects\\Didenko_18IT18_RPM\\src\\algorithm\\Fibonacci.java");
        List<String> list = Files.readAllLines(path, UTF_8);

        return String.valueOf(list);
    }

    static String regular(String result) {
        Pattern pattern = Pattern.compile("([A-Za-z$_](\\w*))");
        Matcher matcher = pattern.matcher(result);
        String answer = "";
        while (matcher.find()) {
            answer += matcher.group();
        }
        return answer;
    }

    private static String regular1( String result) {
        Pattern pattern = Pattern.compile("([A-Za-z$_](\\w*))");
        Matcher matcher = pattern.matcher(result);
        HashSet<String> answer = new HashSet<>();
        while (matcher.find()) {
            answer.add(matcher.group());
        }
        return answer.toString();
    }
}
