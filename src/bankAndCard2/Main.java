package bankAndCard2;

import java.util.Scanner;

/**
 * Класс, демонстрирующий работу банка
 *
 * @author А.В.Диденко 18ИТ18
 */
public class Main {

    public static void main(String[] args) {

        final Account accountVTB = new Account("8562953108437851", "Didenko Aleksandr");
        final Account accountSBER = new Account("4522984017844289", "Didenko Aleksandr");

        final Account.Card Maestro = accountVTB.new Card("2200 2785 3489 2350", 5000);
        final Account.Card Mir = accountSBER.new Card("3340 7854 1975 5433", 6000);
        final Account.Card Mastercard = accountSBER.new Card("3340 9872 5483 9207", 10000);

        Account.Card[] arrayCard = {Maestro, Mastercard, Mir};
        cardSelection(arrayCard);
    }

    /**
     * Метод, который вызывает операцию, номер которой ввёл пользователь
     * @param arrayCard - массив со всеми картами банков
     */
    public static void cardSelection(Account.Card[] arrayCard) {
        balance(arrayCard);
        System.out.println();
        int choise;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Выберите операцию: \n 0. Завершить сеанс \n 1. Снять деньги \n 2. Положить деньги");
            choise = sc.nextInt();
            if (choise == 1 || choise == 2) {
                System.out.println("Выберите карту: \n 1. Maestro \n 2. Mastercard \n 3. Mir");
                int numberCard = sc.nextInt() - 1;
                operation(arrayCard[numberCard], choise);
            }
        } while (choise > 0 && choise < 3);
    }

    /**
     * Метод, выполняющий операци над выбранной картой пользователся
     * @param choise - выбранная операция
     */
    public static void operation(Account.Card arrayCard, int choise){
        System.out.println("Введите сумму: ");
        Scanner sc = new Scanner(System.in);
        int amount = sc.nextInt();
        if (choise == 1) {
            if (errorResponse(arrayCard.withdraw(amount)) == 1) {
                System.out.println("Сумма, которую вы сняли: " + amount + " | Сумма, которая осталась на балансе: " + arrayCard.getAmount());
            }
        }
        if (choise == 2) {
            if (errorResponse(arrayCard.put(amount)) == 1) {
                System.out.println("Сумма, на которую вы пополнили: " + amount + " | Сумма, которая стала на балансе после пополнения: " + arrayCard.getAmount());
            }
        }
    }

    /**
     * Метод, который выводит баланс всех карт
     */
    public static void balance(Account.Card[] arrayCard){
        System.out.println("Баланс VTB: " + arrayCard[0].getAmount());
        System.out.println("Баланс сбербанка (Mastercard): " + arrayCard[1].getAmount());
        System.out.println("Баланс сбербанка (Mir): " + arrayCard[2].getAmount());
    }

    /**
     * @param error - код ошибки или возвращаемая сумма
     */
    private static int errorResponse(int error) {
        int num = 1;
        if (error == 1) {
            System.out.println("Ошибка! Введите другую сумму!");
            return 0;
        }
        if (error != 0) {
            System.out.println("Такой суммы на балансе нет!");
            return 0;
        }
        return num;
    }
}
