package cafeIceCream;

public class Powder extends Decorator {
    public Powder(Desert dessert) {
        super(dessert);
    }

    @Override
    public void withTopping() {
        System.out.println("с посыпкой");
    }
}