package cafeIceCream;

public class Syrup extends Decorator {
    public Syrup(Desert dessert) {
        super(dessert);
    }

    @Override
    public void withTopping() {
        System.out.println("с сиропом");
    }
}
