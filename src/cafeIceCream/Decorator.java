package cafeIceCream;

public abstract class Decorator implements Desert {
    private Desert dessert;

    public Decorator(Desert dessert) {
        this.dessert = dessert;
    }
    public abstract void withTopping();

    public void topping(){
        dessert.topping();
        withTopping();
    }
}
