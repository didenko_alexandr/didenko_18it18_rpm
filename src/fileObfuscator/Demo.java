package fileObfuscator;

import java.util.Scanner;

public class Demo {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите путь к исходному файлу: ");
        String path = scanner.nextLine();
        System.out.println("Введите путь для сохранения результата: ");
        String newPath = scanner.nextLine();
        FileObfuscator obfuscator = new FileObfuscator(path, newPath);
        obfuscator.obfuscate();
    }
}
