package refactoring;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс, демонстрирующий поиск индекса введённой константы
 *
 * @author А.В.Диденко 18ИТ18
 */
public class Refactoring {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int[] array = new int[5];
        random(array);
        System.out.println(Arrays.toString(array));

        System.out.println("Введите элемент, индекс которого хотите найти: ");
        int number = sc.nextInt();
        if (searchIndex(array, number) == -1) {
            System.out.println("Нет такого значения!");
        }
        else {
            System.out.println("Индекс элемента в последовательности - " + searchIndex(array, number));
        }
    }

    /**
     * Метод, заплняющий массив рандомными значениями
     *
     * @param array массив
     */
    private static void random(int[] array) {
        for (int i = 0; i < 5; i++) {
            array[i] = -25 + (int) (Math.random() * 51);
        }
    }

    /**
     * Метод, реализующий поиск индекса введённого элемента
     *
     * @param array массив
     */
    private static int searchIndex(int[] array, int number) {
        int index = -1;
        for (int i = 0; i < 5; i++) {
            if (array[i] == number) {
                index = i;
            }
        }
        return index;
    }
}

