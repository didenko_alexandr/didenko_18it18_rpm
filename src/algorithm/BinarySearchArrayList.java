package algorithm;

import java.util.ArrayList;
import java.util.Scanner;

public class BinarySearchArrayList {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        int item, last;
        int first = 0;
        ArrayList<Integer> array = new ArrayList<>();

        array.add(-7);
        array.add(-5);
        array.add(0);
        array.add(5);
        array.add(9);
        array.add(12);
        array.add(18);
        array.add(23);
        array.add(35);
        array.add(48);
        array.add(56);
        array.add(73);
        array.add(89);
        array.add(97);
        array.add(104);

        System.out.println("Введите элемент для поиска: ");
        item = sc.nextInt();
        last = array.size() - 1;

        int index = binarySearch(array, first, last, item);
        if(index != -1){
            System.out.println(item + " находится под индексом " + binarySearch(array, first, last, item));
        }
        if(index == -1){
            System.out.println("Такого элемента в массиве нет!");
        }

    }

    public static int binarySearch(ArrayList<Integer> array, int first, int last, int item) {
        int mid;

        mid = (first + last) / 2;

        while ((array.get(mid) != item) && (first <= last)) {
            if (array.get(mid) > item) {
                last = mid - 1;
            } else {
                first = mid + 1;
            }
            mid = (first + last) / 2;
        }
        if (first <= last) {
            return mid;
        }
        return -1;
    }
}
