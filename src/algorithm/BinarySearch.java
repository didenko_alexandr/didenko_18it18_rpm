package algorithm;

import java.util.Scanner;

public class BinarySearch {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        int item, last;
        int first = 0;
        int[] array = {-7, -5, 0, 5, 9, 12, 18, 23, 35, 48, 56, 73, 89, 97, 104};

        System.out.println("Введите элемент для поиска: ");
        item = sc.nextInt();
        last = array.length - 1;

        int index = binarySearch(array, first, last, item);
        if(index != -1){
            System.out.println(item + " находится под индексом " + binarySearch(array, first, last, item));
        }
        if(index == -1){
            System.out.println("Такого элемента в массиве нет!");
        }

    }

    public static int binarySearch(int[] array, int first, int last, int item) {
        int mid;

        mid = (first + last) / 2;

        while ((array[mid] != item) && (first <= last)) {
            if (array[mid] > item) {
                last = mid - 1;
            } else {
                first = mid + 1;
            }
            mid = (first + last) / 2;
        }
        if (first <= last) {
            return mid;
        }
        return -1;
    }
}
