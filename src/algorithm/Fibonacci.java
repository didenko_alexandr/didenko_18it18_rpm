package algorithm;

import java.util.Scanner;

public class Fibonacci {

        public static Scanner sc = new Scanner(System.in);

        public static void main(String[] args) {
            System.out.println("Введите число - ");
            int n = sc.nextInt();
            System.out.println("Число Фибоначчи - " + searchFibanacci(n));
            System.out.println("Число Фибоначии рекурсией - " + reverseFibonacci(n));
        }

        public static int searchFibanacci (int n) {
            int a = 0;
            int b = 1;
            for (int i = 2; i <= n; ++i) {
                int next = a + b;
                a = b;
                b = next;
            }
            time();
            return b;
        }

        public static int reverseFibonacci(int n) {
            if (n == 0) {
                return 0;
            } else if (n == 1) {
                return 1;
            } else {
                return reverseFibonacci(n - 1) + reverseFibonacci(n - 2);
            }
        }

        public static void time () {
            long before = System.currentTimeMillis();
            long after = System.currentTimeMillis();
            System.out.println(after - before + " мс");
        }
}
