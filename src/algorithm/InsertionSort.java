package algorithm;

import java.util.ArrayList;
import java.util.Arrays;

    public class InsertionSort {
        public static void main(String[] args) {
            int[] array = {5, 8, 2, 3, 9, 1, 7, 4, 6};

            System.out.println("Исходный массив: ");
            System.out.println(Arrays.toString(array));

            insertionSort(array);
            System.out.println("Отсортированный массив: ");
            System.out.println(Arrays.toString(array));

            ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(5, 8, 2, 3, 9, 1, 7, 4, 6));

            System.out.println("Исходный ArrayList: ");
            System.out.println(arrayList);

            insertionSort(arrayList);
            System.out.println("Отсортированный ArrayList: ");
            System.out.println(arrayList);
        }


        private static void insertionSort(int[] array) {
            for (int out = 1; out < array.length; out++) {
                int temp = array[out];
                int in = out;
                while (in > 0 && array[in - 1] >= temp) {
                    array[in] = array[in - 1];
                    in--;
                }
                array[in] = temp;
            }
        }


        private static void insertionSort(ArrayList<Integer> arrayList) {
            for (int out = 1; out < arrayList.size(); out++) {
                int temp = arrayList.get(out);
                int in = out;
                while (in > 0 && arrayList.get(in - 1) >= temp) {
                    arrayList.set(in, arrayList.get(in - 1));
                    in--;
                }
                arrayList.set(in, temp);
            }
        }
}
