package algorithm;

import java.util.Arrays;

public class QuickSort {
    public static void main(String[] args) {
        int[] x = { 8, 3, 5, -1, 2, 9, 0, 4, 10, 6, 1, 7 };

        int left = 0;
        int right = x.length - 1;

        quickSort(x, left, right);
        System.out.println(Arrays.toString(x));
    }
    public static void quickSort(int[] x, int left, int right) {
        int leftMarker = left;
        int rightMarker = right;
        int support = x[(leftMarker + rightMarker) / 2];
        do {

            while (x[leftMarker] < support) {
                leftMarker++;
            }

            while (x[rightMarker] > support) {
                rightMarker--;
            }

            if (leftMarker <= rightMarker) {

                if (leftMarker < rightMarker) {
                    int tmp = x[leftMarker];
                    x[leftMarker] = x[rightMarker];
                    x[rightMarker] = tmp;
                }

                leftMarker++;
                rightMarker--;
            }
        } while (leftMarker <= rightMarker);

        if (leftMarker < right) {
            quickSort(x, leftMarker, right);
        }
        if (left < rightMarker) {
            quickSort(x, left, rightMarker);
        }
    }
}
