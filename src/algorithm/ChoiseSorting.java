package algorithm;

import java.util.Arrays;

public class ChoiseSorting {

        public static void main(String[] args) {
            int[] array = {7, 1, 4, 2, 6, 9, 3, 5, 8};

            selectionSort(array);
        }
        public static void selectionSort(int[] array) {
            for (int number = 0; number < array.length; number++) {
                int min = array[number];
                int minNum = number;
                for (int num = number + 1; num < array.length; num++) {
                    if (array[num] < min) {
                        min = array[num];
                        minNum = num;
                    }
                }

                int replace = array[number];
                array[number] = array[minNum];
                array[minNum] = replace;
            }
            System.out.println(Arrays.toString(array));
        }
    }
