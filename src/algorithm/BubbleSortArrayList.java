package algorithm;

import java.util.ArrayList;
import java.util.Arrays;

public class BubbleSortArrayList {
    public static void main(String[] args) {
        ArrayList<Integer> array = new ArrayList<>();

        array.add(6);
        array.add(2);
        array.add(7);
        array.add(3);
        array.add(9);
        array.add(1);
        array.add(4);
        array.add(8);
        array.add(10);
        array.add(5);

        bubbleSort(array);
    }

    private static void bubbleSort(ArrayList<Integer> array) {

        for (int i = array.size() - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {

                if (array.get(j) > array.get(j + 1)) {
                    int replace = array.get(j);
                    array.set(j, array.get(j + 1));
                    array.set(j + 1, replace);
                }
            }
        }
        System.out.println(Arrays.toString(new ArrayList[]{array}));
    }
}