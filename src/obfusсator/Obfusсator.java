package obfusсator;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Obfusсator {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        Path path = way();
        String str = String.valueOf(path);
        System.out.println(path);

        System.out.println("Введите новое имя файла, без расширения: ");
        String newFileName = scanner.next();

        String string = "";

        try {
            string = reading(path);
            System.out.println(string);
        } catch (IOException e) {
            System.out.println("Файл не найден!");
        }

        string = deletingComments(string);
        System.out.println(string);

        string = removingSpaces(string);
        System.out.println(string);

        String strName = gettingAname(path);
        System.out.println(strName);

        String nameClass = nameClass(strName);
        System.out.println(nameClass);

        Path newPath = newPath(str, nameClass, newFileName);
        System.out.println(newPath);

        string = name(string, nameClass, newFileName);
        System.out.println(string);

        try {
            newWrite(string, newPath);
        } catch (IOException e) {
            System.out.println("Ошибка!");
        }

    }

    private static Path way() {
        System.out.println("Введите путь к файлу: ");
        Path path = Paths.get(scanner.next());
        return path;
    }

    private static String reading(Path path) throws IOException {
        List<String> list;
        list = Files.readAllLines(path, UTF_8);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            builder.append(list.get(i));
        }
        return builder.toString();
    }

    private static String deletingComments(String string) {
        string = string.replaceAll("(\\/\\*([^*]|[\\r\\n]|(\\*+([^*\\/]|[\\r\\n])))*\\*+\\/)", "");
        string = string.replaceAll("\\/\\/(([А-Яа-я\\w]+\\s{2,})|([А-Яа-я\\s\\w]+))\\s{2,}", "");
        return string;
    }

    private static String removingSpaces(String string) {
        string = string.replaceAll("(\\s{2,})", "");
        return string;
    }

    private static String gettingAname(Path path) {
        Path p = path.getFileName();
        return p.toString();

    }

    private static String nameClass(String strName) {
        strName = strName.replaceAll("\\.\\w*", "");
        return strName;
    }

    private static Path newPath(String str, String nameClass, String newFileName) {
        str = str.replaceAll(nameClass, newFileName);
        Path newPath = Paths.get(str);
        return newPath;
    }

    private static String name(String string, String nameClass, String newFileName) {
        string = string.replaceAll(nameClass, newFileName);
        return string;
    }

    private static void newWrite(String string, Path newPath) throws IOException {
        List<String> lines = Arrays.asList(string);
        Files.write(newPath, lines, Charset.forName("UTF-8")).toString();

    }
}
