package sortingObjects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Person person1 = new Person("Головин", 1998);
        Person person2 = new Person("Алапаев", 2002);
        Person person3 = new Person("Шмыгин", 1990);
        Person person4 = new Person("Воробьёва", 2010);
        Person person5 = new Person("Климова", 2006);

        ArrayList<Person> person = new ArrayList<>();

        person.add(person1);
        person.add(person2);
        person.add(person3);
        person.add(person4);
        person.add(person5);

        ArrayList<String> personSurname = new ArrayList<>();

        personSurname.add(person1.getSurname());
        personSurname.add(person2.getSurname());
        personSurname.add(person3.getSurname());
        personSurname.add(person4.getSurname());
        personSurname.add(person5.getSurname());

        sortSurname(personSurname);
        sortYear(person);

        System.out.println(surnameSearch(person));
    }

    public static void sortSurname(ArrayList<String> personSurname) {

        Collections.sort(personSurname);
        System.out.println("Сортировка по алфавиту: ");
        for (String counter : personSurname) {
            System.out.println(counter);
        }
        System.out.println(" ");
    }

    public static void sortYear(ArrayList<Person> person) {
        for (int number = 0; number < person.size(); number++) {
            int max = person.get(number).getBirthYear();
            int maxNum = number;
            for (int num = number + 1; num < person.size(); num++) {
                if (person.get(num).getBirthYear() > max) {
                    max = person.get(num).getBirthYear();
                    maxNum = num;
                }
            }

            Person replace = person.get(number);
            person.set(number, person.get(maxNum));
            person.set(maxNum, replace);
        }
        System.out.println("Сортировка по дате рождения в порядке убывания: " + Arrays.toString(new ArrayList[]{person}));
        System.out.println(" ");
    }

    public static Object surnameSearch(ArrayList<Person> person) {
        System.out.println("Введите фамилию для поиска: ");
        String index = sc.nextLine();
        for (int i = 0; i < person.size(); i++) {
            if (person.get(i).getSurname().equals(index)) {
                return person.get(i);
            }
        }
        return "Такого нет!";
    }
}
