package bankAndCard;

import java.util.Scanner;

public class Main {

    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        final Account accountVTB = new Account("8562953108437851", "Didenko Aleksandr");
        final Account.Card Maestro = accountVTB.new Card("2200 2785 3489 2350", 5000);

        final Account accountSBER = new Account("4522984017844289", "Didenko Aleksandr");
        final Account.Card Mir = accountSBER.new Card("3340 7854 1975 5433", 3000);
        final Account.Card Mastercard = accountSBER.new Card("3340 9872 5483 9207", 10000);
        operation(accountVTB,accountSBER, Mir, Maestro, Mastercard);
    }

    public static void operation(Account accountVTB,Account accountSBER,Account.Card Mir,Account.Card Maestro,Account.Card Mastercard) {
        int selectOperation = 0;
        do {
            System.out.println("Баланс VTB: " + Maestro.getAmount());
            System.out.println("Баланс сбербанка (Mir): " + Mir.getAmount());
            System.out.println("Баланс сбербанка (Mastercard): " + Mastercard.getAmount());

            System.out.println("Что вы хотите сделать? \n 1. Положить деньги на сбербанк \n 2. Вывести деньги со сбербанка \n 3. Положить деньги на VTB \n 4. Вывести деньги с VTB");
            selectOperation = sc.nextInt();

            if (selectOperation == 1) {
                int cardSber = 0;
                do {
                    System.out.println("Выберите карту: \n 0. Вернуться назад \n 1. Mir \n 2. Mastercard");
                    cardSber = sc.nextInt();
                    if (cardSber == 1) {
                        System.out.println("Введите сумму, которую вы хотите положить: ");
                        int put = sc.nextInt();
                        if (errorResponse(Mir.put(put)) == 1) {
                            System.out.println("Сумма, на которую вы пополнили: " + put + " | Сумма, которая стала на балансе после пополнения: " + Mir.getAmount());
                        }
                    }
                    if (cardSber == 2) {
                        System.out.println("Введите сумму, которую вы хотите положить: ");
                        int put = sc.nextInt();
                        if (errorResponse(Mastercard.put(put)) == 1) {
                            System.out.println("Сумма, на которую вы пополнили: " + put + " | Сумма, которая стала на балансе после пополнения: " + Mastercard.getAmount());
                        }
                    }
                } while (cardSber > 0 && cardSber < 2);
            }

            if (selectOperation == 2) {
                int sberCard = 0;
                do {
                    System.out.println("Выберите карту: \n 0. Вернуться назад \n 1. Mir \n 2. Mastercard");
                    sberCard = sc.nextInt();
                    if (sberCard == 1) {
                        System.out.println("Сколько вы хотите снять?");
                        int withdraw = sc.nextInt();
                        if (errorResponse(Mir.withdraw(withdraw)) == 1) {
                            System.out.println("Сумма, которую вы сняли: " + withdraw + " | Сумма, которая осталась на балансе: " + Mir.getAmount() + "\n");
                        }
                    }
                    if (sberCard == 2) {
                        System.out.println("Сколько вы хотите снять?");
                        int withdraw = sc.nextInt();
                        if (errorResponse(Mastercard.withdraw(withdraw)) == 1) {
                            System.out.println("Сумма, которую вы сняли: " + withdraw + " | Сумма, которая осталась на балансе: " + Mastercard.getAmount() + "\n");
                        }
                    }
                } while (sberCard > 0 && sberCard < 2);
            }

            if (selectOperation == 3) {
                System.out.println("Введите сумму, которую вы хотите положить: ");
                int putVTB = sc.nextInt();
                if (errorResponse(Maestro.put(putVTB)) == 1) {
                    System.out.println("Сумма, на которую вы пополнили: " + putVTB + " | Сумма, которая стала на балансе после пополнения: " + Maestro.getAmount());
                }
            }

            if (selectOperation == 4) {
                System.out.println("Сколько вы хотите снять? ");
                int withdrawVTB = sc.nextInt();
                if (errorResponse(Maestro.withdraw(withdrawVTB)) == 1) {
                    System.out.println("Сумма, которую сняли: " + withdrawVTB + " | Сумма, которая осталась на балансе: " + Maestro.getAmount());
                }
            }
        } while (selectOperation > 0 && selectOperation < 4);
    }

    private static int errorResponse(int methodOperation) {
        int num = 1;
        if (methodOperation == 1) {
            System.out.println("Введите другую сумму!");
            return 0;
        }
        if (methodOperation == 2) {
            System.out.println("Столько на балансе нет!");
            return 0;
        }
        return num;
    }
}

