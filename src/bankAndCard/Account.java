package bankAndCard;

public class Account {
    private final String number;
    private final String owner;

    public Account(final String number, final String owner) {
        this.number = number;
        this.owner = owner;
    }

    public String getNumber() {
        return number;
    }

    public String getOwner() {
        return owner;
    }

    class Card {
        private final String number;
        private int amount;

        public Card(final String number, int amount) {
            this.number = number;
            this.amount = amount;
        }
        public String getNumber() {
            return number;
        }
        public int getAmount() {
            return amount;
        }

        int withdraw(int amountToWithdraw) {
            int error = 0;
            if (amountToWithdraw < 0) {
                error = 1;
            }
            if (amountToWithdraw > amount) {
                final int amountToReturn = amount;
                amount = 0;
                return 2;
            }
            amount = amount - amountToWithdraw;
            return error;
        }

        int put(int amountToPut) {
            int error = 0;
            if (amountToPut < 0) {
                return 1;
            }
            amount = amount + amountToPut;
            return error;
        }

    }
}

